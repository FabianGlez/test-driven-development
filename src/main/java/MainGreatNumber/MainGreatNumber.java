package MainGreatNumber;

import List.GreatNumber;

/**
 * Metodo principal a la llamada al programa
 *
 */
public class MainGreatNumber {
	public static void main(String args[]) {
		GreatNumber greatNumber = new GreatNumber();
		greatNumber.quickSort();
	}
}

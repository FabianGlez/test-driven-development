package List;

import java.util.Scanner;

public class GreatNumber {
	/**
	 * Atributos
	 */
	private final int vec[]; // vector de tipo entero
	private final int size = 5; // tamaño de nuestro vector

	/**
	 * Constructor
	 */
	public GreatNumber() {
		vec = new int[size];
	}

	/**
	 * Metodo para llenar el vector
	 */
	public void load() {
		for (int i = 0; i < size; i++) {
			// creamos el objeto para lectura
			Scanner scan = new Scanner(System.in);
			System.out.println("Dame un valor para posicion:" + i);
			// En esta etapa se almacena el dato en el vector segun el indice
			vec[i] = scan.nextInt();
		}
	}

	/**
	 * Metodo para ordenar los elementos del vector
	 */
	public void quickSort() {
		// cargamos los datos haciendo la llamada al metodo 'load'
		load();
		int left = 0;
		int right = 0;
		int pivot = 0;
		while (left < vec.length) {
			while (right < vec.length) {
				if (vec[left] >= vec[right]) {
					right++;
				} else {
					pivot = vec[left];
					vec[left] = vec[right];
					vec[right] = pivot;
					left = 0;
					right = left + 1;
					System.out.print("===== Vector ordenado: =====");
					System.out.println();
					// mostramos los datos del vector haciendo el llamado al
					// metodo 'show'
					show();
					// Hacemos la llamada al metodo 'greatNumber' para que nos
					// muestre la cifra mayor
					greatNumber();
				}
			}
			left++;
			right = left + 1;
		}
	}

	/**
	 * Metodo para mostrar el contenido del vector
	 */
	public void show() {
		for (int i = 0; i < size; i++) {
			System.out.println("Valor del vector en la posicion" + i + "= vec["
					+ vec[i] + "]");
		}
	}

	/**
	 * Metodo para mostrar la cifra mayor
	 */
	public void greatNumber() {
		String digit = "";
		for (int i = 0; i < size; i++) {
			digit = digit + vec[i];
			System.out.println("El numero mayor es:" + digit);
		}
	}

}
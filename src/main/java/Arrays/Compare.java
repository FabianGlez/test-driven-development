package Arrays;

/**
 * 
 * @author hunk
 *
 */
public class Compare {
	/**
	 * Metodo para comparar si dos arreglos de caracteres son iguales
	 * 
	 * @param cad1
	 * @param cad2
	 * @return
	 */
	public final boolean compare(final char cad1[], final char cad2[]) {
		if (cad1.length != cad2.length) {
			return false;
		}
		for (int i = 0; i < cad1.length; i++) {
			if (cad1[i] != cad2[i]) {
				return false;
			}
		}
		return true;
	}
}

package Arrays;

/**
 * 
 * @author hunk
 *
 */

public class Split {
	/**
	 * Metodo para dividir un arreglo de cadenas
	 * 
	 * @param phrase
	 * @param delimiter
	 * @return
	 */
	public String[] split(String phrase, char delimiter) {
		int cont = 0;
		int index = 0;
		for (int i = 0; i < phrase.length(); i++) {
			if (phrase.charAt(i) == delimiter) {
				cont++;
			}
		}
		String[] part = new String[cont + 1];
		for (int i = 0; i < part.length; i++) {
			part[i] = "";
		}

		for (int i = 0; i < phrase.length(); i++) {
			if (phrase.charAt(i) == delimiter) {
				index++;
				continue;
			}
			part[index] = part[index] + phrase.charAt(i);
		}
		return part;
	}
}

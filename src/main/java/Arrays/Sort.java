package Arrays;

/**
 * 
 * @author hunk
 *
 */
public class Sort {
	/**
	 * Este metodo ordena en forma alfabetica un arreglo de caracteres
	 * 
	 * @param word
	 * @return
	 */
	public final char[] sort(final char word[]) {
		while (bubleSort(word)) {
		}
		return word;
	}

	public final boolean bubleSort(final char word[]) {
		boolean flag = false;
		char letter;
		for (int i = 0; i < word.length - 1; i++) {
			if (word[i] > word[i + 1]) {
				letter = word[i];
				word[i] = word[i + 1];
				word[i + 1] = letter;
				flag = true;
			}
		}
		return flag;
	}
}

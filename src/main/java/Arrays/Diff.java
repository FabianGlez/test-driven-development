package Arrays;

/**
 * 
 * @author hunk
 *
 */
public class Diff {
	/**
	 * Metodo para diferenciar los caracteres y longitud de un arreglo
	 * 
	 * @param phrase1
	 * @param phrase2
	 * @return
	 */
	public final String[] diff(final char phrase1[], final char phrase2[]) {
		int maxLength = 0;
		String words = "";

		if (phrase1.length > phrase2.length) {
			maxLength = phrase1.length;
		} else {
			maxLength = phrase2.length;
		}

		String[] differences = new String[maxLength];
		for (int index = 0; index < differences.length; index++) {
			if (index > phrase1.length - 1) {
				words = "<" + phrase2[index];
				differences[index] = words;
				continue;
			}
			if (index > phrase2.length - 1) {
				words = phrase1[index] + ">";
				differences[index] = words;
				continue;
			}
			if (phrase1[index] == phrase2[index]) {
				differences[index] = "";
			} else {
				words = phrase1[index] + "<>" + phrase2[index];
				differences[index] = words;
			}
		}
		return differences;
	}
}

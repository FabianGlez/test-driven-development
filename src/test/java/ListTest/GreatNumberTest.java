package ListTest;

import static org.junit.Assert.*;
import List.List_Sort;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class List_Sort_Test {
	// Creamos la instancia de la clase a probar
	private List_Sort list_sort;

	@Before
	public void setUp() {
		list_sort = new List_Sort();
	}

	@Test
	public void firstScenario() {
		String[] numberList = { "80", "2", "1", "9" };
		String[] sortNumber = list_sort.sortBiggerNumber(numberList);
		Assert.assertEquals("9", sortNumber[0]);
		Assert.assertEquals("80", sortNumber[1]);
		Assert.assertEquals("2", sortNumber[2]);
		Assert.assertEquals("1", sortNumber[3]);
	}

	@Test
	public void secondScenario() {
		String[] numberList = { "80219" };
		String[] sortNumber = list_sort.sortBiggerNumber(numberList);
		Assert.assertEquals("98021", sortNumber[0]);
	}

	@Test
	public void thirdScenario() {
		String[] numberList = { "5", "5", "5" };
		String[] sortNumber = list_sort.sortBiggerNumber(numberList);
		Assert.assertEquals("5", sortNumber[0]);
		Assert.assertEquals("5", sortNumber[1]);
		Assert.assertEquals("5", sortNumber[2]);
	}
	
	@Test
	public void fourthScenario() {
		String[] numberList = {"420", "42", "423"};
		String[] sortNumber = list_sort.sortBiggerNumber(numberList);
		Assert.assertEquals("424", sortNumber[0]);
		Assert.assertEquals("234", sortNumber[1]);
		Assert.assertEquals("20", sortNumber[2]);
	}
}
package ArraysTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import Arrays.Diff;

/**
 * 
 * @author hunk
 *
 */
public class DiffTest {
	// se crea la instancia de la clase Diff
	private Diff diff_;

	@Before
	public void setUp() {
		diff_ = new Diff();
	}

	@Test
	public void diffTest() {
		char[] letters1 = { 'b' };
		char[] letters2 = { 'b' };
		String[] phrase = diff_.diff(letters1, letters2);
		Assert.assertEquals("", phrase[0]);
	}

	@Test
	public void diffTest1() {
		char[] letters1 = { 'b' };
		char[] letters2 = { 'h' };
		String[] phrase = diff_.diff(letters1, letters2);
		Assert.assertEquals("b<>h", phrase[0]);
	}

	@Test
	public void diffTest2() {
		char[] letters1 = { 'b', 'a' };
		char[] letters2 = { 'b' };
		String[] phrase = diff_.diff(letters1, letters2);
		Assert.assertEquals("", phrase[0]);
		Assert.assertEquals("a>", phrase[1]);
	}

	@Test
	public void diffTest3() {
		char[] letters1 = { 'b' };
		char[] letters2 = { 'b', 'a' };
		String[] phrase = diff_.diff(letters1, letters2);
		Assert.assertEquals("", phrase[0]);
		Assert.assertEquals("<a", phrase[1]);
	}

	@Test
	public void diffTest4() {
		char[] letters1 = { 'a', 'b' };
		char[] letters2 = { 'a', 'x' };
		String[] phrase = diff_.diff(letters1, letters2);
		Assert.assertEquals("", phrase[0]);
		Assert.assertEquals("b<>x", phrase[1]);
	}
}

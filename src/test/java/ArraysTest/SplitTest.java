package ArraysTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import Arrays.Split;

/**
 * 
 * @author hunk
 *
 */

public class SplitTest {
	// creamos una instancia de la clase Character
	private Split character;

	@Before
	public void setUp() {
		character = new Split();
	}

	@Test
	public void split() {
		String string = "¿En,qué,se,parece,un,abogado,a,un,hacker?,";
		char delimiter = ',';
		String[] word = character.split(string, delimiter);
		Assert.assertEquals("¿En", word[0]);
		Assert.assertEquals("qué", word[1]);
		Assert.assertEquals("se", word[2]);
		Assert.assertEquals("parece", word[3]);
		Assert.assertEquals("un", word[4]);
		Assert.assertEquals("abogado", word[5]);
		Assert.assertEquals("a", word[6]);
		Assert.assertEquals("un", word[7]);
		Assert.assertEquals("hacker?", word[8]);
		// Assert.assertEquals(9, word.length);
	}

	@Test
	public void splitBegin() {
		String string = ",¿En,qué,se,parece,un,abogado,a,un,hacker?";
		char delimiter = ',';
		String[] word = character.split(string, delimiter);
		Assert.assertEquals("", word[0]);
		Assert.assertEquals("¿En", word[1]);
		Assert.assertEquals("qué", word[2]);
		Assert.assertEquals("se", word[3]);
		Assert.assertEquals("parece", word[4]);
		Assert.assertEquals("un", word[5]);
		Assert.assertEquals("abogado", word[6]);
		Assert.assertEquals("a", word[7]);
		Assert.assertEquals("un", word[8]);
		Assert.assertEquals("hacker?", word[9]);
		Assert.assertEquals(10, word.length);
	}

	@Test
	public void splitCharacter() {
		String string = "¿En,qué,se,parece,un,abogado,a,un,hacker?";
		char delimiter = '|';
		String[] word = character.split(string, delimiter);
		Assert.assertEquals(string, word[0]);
		Assert.assertEquals(1, word.length);
	}

	@Test
	public void splitEmpty() {
		String string = "";
		char delimiter = ',';
		String[] word = character.split(string, delimiter);
		Assert.assertEquals(string, word[0]);
		Assert.assertEquals(1, word.length);
	}

	@Test
	public void arraySizeSplit() {
		String string = "¿En qué se parece un abogado a un hacker?";
		char delimiter = ' ';
		String[] word = character.split(string, delimiter);
		Assert.assertEquals(9, word.length);
	}
}

package ArraysTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import Arrays.Sort;

/**
 * 
 * @author hunk
 *
 */
public class SortTest {
	// Creamos la instancia de la clase Sort
	private Sort sort_;

	@Before
	public void setUp() {
		sort_ = new Sort();
	}

	@Test
	public void sortTest() {
		char[] word = { 'u', 'n', 'i', 'x' };
		char[] orderletter = sort_.sort(word);
		Assert.assertEquals('i', orderletter[0]);
		Assert.assertEquals('n', orderletter[1]);
		Assert.assertEquals('u', orderletter[2]);
		Assert.assertEquals('x', orderletter[3]);
	}

	@Test
	public void sortTest1() {
		char[] word = { 'l', 'e', 's', 's' };
		char[] orderletter = sort_.sort(word);
		Assert.assertEquals('e', orderletter[0]);
		Assert.assertEquals('l', orderletter[1]);
		Assert.assertEquals('s', orderletter[2]);
		Assert.assertEquals('s', orderletter[3]);
	}

}

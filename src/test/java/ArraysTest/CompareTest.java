package ArraysTest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import Arrays.Compare;

/**
 * 
 * @author hunk
 *
 */
public class CompareTest {
	// creamos la instancia de la clase Compare
	private Compare compare;

	@Before
	public void setUp() {
		compare = new Compare();
	}

	@Test
	public void compareTrueTest() {
		char[] letters = { 'a', 'e', 'i', 'o', 'u' };
		char[] otherLetters = { 'a', 'e', 'i', 'o', 'u' };
		assertTrue(compare.compare(letters, otherLetters));
	}

	@Test
	public void compareFalseTest() {
		char[] letters = { 'a', 'e', 'x', 'o', 'u' };
		char[] otherLetters = { 'a', 'e', 'i', 'o', 'u' };
		assertFalse(compare.compare(letters, otherLetters));
	}

	@Test
	public void compareEmptyTest() {
		char[] letters = {};
		char[] otherLetters = {};
		assertTrue(compare.compare(letters, otherLetters));
	}

	@Test
	public void compareArrayLenght() {
		char[] letters = { 'a', 'e', 'i', 'o', 'u' };
		char[] otherLetters = { 'a', 'e', 'i', 'o' };
		assertFalse(compare.compare(letters, otherLetters));
	}

	@Test
	public void compareMayuscula() {
		char[] letters = { 'A', 'E', 'I', 'O', 'U' };
		char[] otherLetters = { 'a', 'e', 'i', 'o', 'u' };
		assertFalse(compare.compare(letters, otherLetters));
	}

	@Test
	public void compareSpecialCharacters() {
		char[] letters = { '@', '#', '$', '&', '%' };
		char[] otherLetters = { 'a', 'e', 'i', 'o', 'u' };
		assertFalse(compare.compare(letters, otherLetters));
	}
}
